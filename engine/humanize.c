
#include <stdlib.h>
#include <memory.h>
#include <math.h>

#include "humanize.h"

static inline int ms_to_samples(float ms, int samplerate) {
  return (int)(ms / 1000.0 * samplerate);
}

static float gauss(void) {
  float x = (float)random() / RAND_MAX,
         y = (float)random() / RAND_MAX,
         z = sqrtf(-2 * logf(x)) * cosf(2 * M_PI * y);
  return z;
}

static float random_normal_distribution(float stddev) {
  return gauss() * stddev;
}

// Initialize a humanizer (which tracks a single instrument).
void humanizer_init(struct humanizer *hum, struct humanizer_settings *settings) {
  hum->settings = settings;
  hum->latency_offset = 0;
  hum->latency_last_pos = 0;
}

// Return the latency (number of samples) for the next event.
int humanize_latency_get_next_offset(struct humanizer *hum, int pos) {
  int latency_max, latency_laid_back;
  float duration, offset_ms;

  latency_max = ms_to_samples(hum->settings->latency_max_ms, hum->settings->samplerate);
  latency_laid_back = ms_to_samples(hum->settings->latency_laid_back_ms, hum->settings->samplerate);

  duration = (float)(pos - hum->latency_last_pos) / hum->settings->samplerate;
  hum->latency_offset *= pow(1.0 - hum->settings->latency_regain, duration);
  hum->latency_last_pos = pos;

  offset_ms = random_normal_distribution(hum->settings->latency_stddev_ms);
  hum->latency_offset += ms_to_samples(offset_ms, hum->settings->samplerate);
  if (hum->latency_offset < -latency_max)
    hum->latency_offset = -latency_max;
  if (hum->latency_offset > latency_max)
    hum->latency_offset = latency_max;

  return latency_max + latency_laid_back + hum->latency_offset;
}

int humanize_velocity(struct humanizer *hum, int velocity) {
  float v;
  int vo;

  v = velocity / 127.0;
  v *= 1 + random_normal_distribution(hum->settings->velocity_stddev);
  vo = v * 127;
  if (vo < 0)
    vo = 0;
  if (vo > 127)
    vo = 127;
  return vo;
}

// Initialize humanizer_settings with default (disabled) values.
void humanizer_settings_init(struct humanizer_settings *settings, int samplerate) {
  settings->samplerate = samplerate;
  settings->latency_regain = 1;
  settings->latency_laid_back_ms = 0;
  settings->latency_max_ms = 0;
  settings->latency_stddev_ms = 0;
  settings->velocity_stddev = 0;
}

// Returns the maximum latency introduced by the humanizer, so that
// plugins can compensate for it. This aligns the humanizer 0 time
// with latency_max_ms.
int humanize_get_latency(struct humanizer_settings *settings) {
  return ms_to_samples(settings->latency_max_ms, settings->samplerate);
}

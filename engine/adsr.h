#ifndef __adsr_H
#define __adsr_H 1

#define ADSR_IDLE 0
#define ADSR_ATTACK 1
#define ADSR_DECAY 2
#define ADSR_SUSTAIN 3
#define ADSR_RELEASE 4

struct adsr {
  int attack, decay, release;
  float sustain;

  int state;
  int ticks;
  float value;
  float release_value;
};

void adsr_trigger(struct adsr *, int, int, float, int);
float adsr_get_value(struct adsr *, float);
void adsr_release(struct adsr *);

#endif

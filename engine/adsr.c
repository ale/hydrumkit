#include "config.h"

#include <stdio.h>

#include "adsr.h"
#include "exponential_tables.h"

static float linear_interpolation_fraction(float a, float b, int num, int den) {
  float val = (float)num / (float)den;
  return a * (1 - val) + b * val;
}

void adsr_trigger(struct adsr *adsr, int attack, int decay, float sustain, int release) {
  // Normalize values.
  if (attack < 0)
    attack = 0;
  if (decay < 0)
    decay = 0;
  if (sustain < 0)
    sustain = 0;
  if (release < 256)
    release = 256;
  if (attack > 100000)
    attack = 100000;
  if (decay > 100000)
    decay = 100000;
  if (sustain > 1.0)
    sustain = 1.0;
  if (release > 100256)
    release = 100256;

  adsr->attack = attack;
  adsr->decay = decay;
  adsr->sustain = sustain;
  adsr->release = release;
  adsr->state = ADSR_ATTACK;
  adsr->ticks = 0;
  adsr->value = adsr->release_value = 0;
}

float adsr_get_value(struct adsr *adsr, float step) {
  float value;

  switch (adsr->state) {
  case ADSR_ATTACK:
    if (adsr->attack == 0) {
      value = 1;
    } else {
      value = convex_exponent(linear_interpolation_fraction(0, 1, adsr->ticks, adsr->attack));
    }
    adsr->ticks += step;
    if (adsr->ticks > adsr->attack) {
      adsr->state = ADSR_DECAY;
      adsr->ticks = 0;
    }
    break;

  case ADSR_DECAY:
    if (adsr->decay == 0) {
      value = adsr->sustain;
    } else {
      value = concave_exponent(linear_interpolation_fraction(1, 0, adsr->ticks, adsr->decay)) * (1 - adsr->sustain) + adsr->sustain;
    }
    adsr->ticks += step;
    if (adsr->ticks > adsr->decay) {
      adsr->state = ADSR_SUSTAIN;
      adsr->ticks = 0;
    }
    break;

  case ADSR_SUSTAIN:
    value = adsr->sustain;
    //adsr->ticks += step;
    break;

  case ADSR_RELEASE:
    value = concave_exponent(linear_interpolation_fraction(1, 0, adsr->ticks, adsr->release)) * adsr->release_value;
    adsr->ticks += step;
    if (adsr->ticks > adsr->release) {
      adsr->state = ADSR_IDLE;
      adsr->ticks = 0;
    }
    break;

  default:
    value = 0;
  }

  adsr->value = value;
  return value;
}

void adsr_release(struct adsr *adsr) {
  if (adsr->state == ADSR_IDLE || adsr->state == ADSR_RELEASE)
    return;
  adsr->release_value = adsr->value;
  adsr->state = ADSR_RELEASE;
  adsr->ticks = 0;
}

#ifndef __drumkit_H
#define __drumkit_H 1

#include <sndfile.h>


struct sample {
  SF_INFO info;
  float *data_l, *data_r;
};

struct layer {
  float min, max;
  float gain;
  struct sample sample;
};

struct instrument {
  int index;
  char *name;
  int note;
  float envelope_attack, envelope_decay,
    envelope_release, envelope_sustain;
  float pan_l, pan_r;
  float gain;
  float volume;
  int mute_group;
  int num_layers;
  struct layer *layers;
};

struct layer *instrument_find_layer_by_velocity(struct instrument *, float);

struct drumkit {
  int num_instruments;
  struct instrument *instruments;
  char *path;
};

struct drumkit *drumkit_new(const char *, int);
int drumkit_load(struct drumkit *, const char *, int);
void drumkit_free(struct drumkit *);
void drumkit_print(struct drumkit *);
struct instrument *drumkit_find_instrument_by_note(struct drumkit *, int);

#endif

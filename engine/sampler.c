#include "config.h"

#include <stdlib.h>

#include "sampler.h"

static void init_voices(struct sampler *sampler) {
  int i;
  for (i = 0; i < sampler->num_voices; i++) {
    voice_init(&sampler->voices[i], sampler->samplerate);
  }
}

struct sampler *sampler_new(int num_voices, int samplerate, struct humanizer_settings *hsettings) {
  struct sampler *sampler = (struct sampler *)calloc(1, sizeof(struct sampler));

  sampler->hsettings = hsettings;
  sampler->samplerate = samplerate;
  sampler->num_voices = num_voices;
  sampler->voices = (struct voice *)calloc(num_voices, sizeof(struct voice));
  init_voices(sampler);
  return sampler;
}

void sampler_free(struct sampler *sampler) {
  if (sampler->kit) {
    drumkit_free(sampler->kit);
    free(sampler->kit);
  }
  free(sampler->voices);
  free(sampler);
}

void sampler_set_drumkit(struct sampler *sampler, struct drumkit *kit) {
  int i;

  if (sampler->kit) {
    // Re-initialize all voices to avoid pending references to freed samples.
    init_voices(sampler);
    drumkit_free(sampler->kit);
    free(sampler->kit);
    free(sampler->humanizers);
  }
  sampler->kit = kit;
  sampler->humanizers = (struct humanizer *)calloc(kit->num_instruments, sizeof(struct humanizer));
  for (i = 0; i < kit->num_instruments; i++) {
    humanizer_init(&sampler->humanizers[i], sampler->hsettings);
  }
  sampler->ticks = 0;
}

void sampler_noteon(struct sampler *sampler, int note, int velocity) {
  struct instrument *ins;
  int i, offset;

  if (!sampler->kit)
    return;
  
  ins = drumkit_find_instrument_by_note(sampler->kit, note);
  if (!ins) {
    fprintf(stderr, "no instrument found for note %d\n", note);
    return;
  }

  // Kill other notes in the same mute_group.
  if (ins->mute_group >= 0) {
    for (i = 0; i < sampler->num_voices; i++) {
      voice_noteoff_if_mute_group(&sampler->voices[i], ins->mute_group);
    }
  }
  
  // Find a free voice.
  struct voice *voice = NULL;
  for (i = 0; i < sampler->num_voices; i++) {
    if (!voice_is_playing(&sampler->voices[i])) {
      voice = &sampler->voices[i];
      break;
    }
  }
  if (!voice) {
    fprintf(stderr, "all voices are busy\n");
    return;
  }
  
  // Humanize: modify preamble and velocity.
  offset = humanize_latency_get_next_offset(&sampler->humanizers[ins->index], sampler->ticks);
  velocity = humanize_velocity(&sampler->humanizers[ins->index], velocity);
  
  voice_noteon(voice, ins, note, velocity, offset);
}

void sampler_noteoff(struct sampler *sampler, int note) {
  int i;

  for (i = 0; i < sampler->num_voices; i++) {
    voice_noteoff_if_note(&sampler->voices[i], note);
  }
}

void sampler_output(struct sampler *sampler, float *out_l, float *out_r, int num_frames) {
  int i;

  // Clear the output buffers.
  for (i = 0; i < num_frames; i++) {
    out_l[i] = out_r[i] = 0;
  }

  // Accumulate output from all the voices.
  for (i = 0; i < sampler->num_voices; i++) {
    voice_process(&sampler->voices[i], out_l, out_r, num_frames);
  }

  sampler->ticks += num_frames;
}

int sampler_get_latency(struct sampler *sampler) {
  return humanize_get_latency(sampler->hsettings);
}

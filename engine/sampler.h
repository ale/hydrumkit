#ifndef __sampler_H
#define __sampler_H 1

#include "drumkit.h"
#include "voice.h"
#include "humanize.h"

struct sampler {
  struct drumkit *kit;
  int samplerate;
  int num_voices;
  struct voice *voices;
  struct humanizer_settings *hsettings;
  struct humanizer *humanizers;
  int ticks;
};

struct sampler *sampler_new(int, int, struct humanizer_settings *);
void sampler_set_drumkit(struct sampler *, struct drumkit *);
void sampler_free(struct sampler *);
void sampler_noteon(struct sampler *, int, int);
void sampler_noteoff(struct sampler *, int);
void sampler_output(struct sampler *, float *, float *, int);
int sampler_get_latency(struct sampler *);

#endif

#ifndef __voice_H
#define __voice_H 1

#include "adsr.h"
#include "drumkit.h"

struct voice {
  int samplerate;

  int playing;
  int in_preamble;
  int preamble_countdown;
  int note;
  int mute_group;

  int ticks;
  float pan_l, pan_r, gain;
  struct adsr adsr;
  struct sample *cur_sample;
};

void voice_init(struct voice *, int);
int voice_noteon(struct voice *, struct instrument *, int, int, int);
int voice_noteoff_if_note(struct voice *, int);
int voice_noteoff_if_mute_group(struct voice *, int);
void voice_process(struct voice *, float *, float *, int);

static inline int voice_is_playing(struct voice *voice) {
  return voice->playing || voice->in_preamble;
}

#endif

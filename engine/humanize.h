#ifndef __humanize_H
#define __humanize_H 1


struct humanizer_settings {
  float samplerate;

  float latency_regain;
  float latency_laid_back_ms;
  float latency_max_ms;
  float latency_stddev_ms;

  float velocity_stddev;
};

struct humanizer {
  struct humanizer_settings *settings;

  float latency_offset;
  int latency_last_pos;
};

void humanizer_init(struct humanizer *, struct humanizer_settings *);
void humanizer_settings_init(struct humanizer_settings *, int);
int humanize_latency_get_next_offset(struct humanizer *, int);
int humanize_velocity(struct humanizer *, int);
int humanize_get_latency(struct humanizer_settings *);

#endif

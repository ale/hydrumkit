#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

#include <libxml/parser.h>
#include <libxml/tree.h>
#include <libxml/xpath.h>
#include <libxml/xpathInternals.h>

#include <samplerate.h>

#include "drumkit.h"

// Set default values on an instrument.
static void instrument_set_defaults(struct instrument *ins) {
  ins->pan_l = ins->pan_r = 1;
  ins->gain = ins->volume = 1;
  ins->envelope_attack = 0;
  ins->envelope_decay = 0;
  ins->envelope_sustain = 1;
  ins->envelope_release = 10000;
  ins->mute_group = -1;
  ins->note = 0;
}

// Return a sndfile format value in human-readable format.
static char *sf_format_name(int format) {
  static char fmtbuf[32];
  const char *major = "weird", *subtype = NULL;
  
  switch (format & SF_FORMAT_TYPEMASK) {
  case SF_FORMAT_WAV: major = "wav"; break;
  case SF_FORMAT_AIFF: major = "aiff"; break;
  case SF_FORMAT_FLAC: major = "flac"; break;
  }
  switch (format & SF_FORMAT_SUBMASK) {
  case SF_FORMAT_PCM_S8: subtype = "s8"; break;
  case SF_FORMAT_PCM_U8: subtype = "u8"; break;
  case SF_FORMAT_PCM_16: subtype = "s16"; break;
  case SF_FORMAT_PCM_24: subtype = "s24"; break;
  case SF_FORMAT_PCM_32: subtype = "s32"; break;
  case SF_FORMAT_FLOAT: subtype = "float"; break;
  case SF_FORMAT_DOUBLE: subtype = "double"; break;
  case SF_FORMAT_VORBIS: subtype = "vorbis"; break;
  }

  strcpy(fmtbuf, major);
  if (subtype) {
    strcat(fmtbuf, "/");
    strcat(fmtbuf, subtype);
  }
  return fmtbuf;
}

// Dump instrument metadata to standard output.
static void print_instrument(struct instrument *ins) {
  int i;
  printf("instrument %s: note=%d, pan=%g/%g, gain=%g, vol=%g", ins->name,
         ins->note, ins->pan_l, ins->pan_r, ins->gain, ins->volume);
  if (ins->mute_group >= 0) {
    printf(", mute_group=%d", ins->mute_group);
  }
  printf("\n");
  for (i = 0; i < ins->num_layers; i++) {
    printf("  + [%f,%f] @%dHz %s/%s\n", ins->layers[i].min, ins->layers[i].max,
           ins->layers[i].sample.info.samplerate,
           (ins->layers[i].sample.info.channels > 1) ? "s" : "m",
           sf_format_name(ins->layers[i].sample.info.format));
  }
}

// Return a string with the content of the result of an XPath
// expression (which should evaluate the text() of a single node), or
// NULL if the expression returned no nodes.
static const char *xpath_value_as_string(xmlNodePtr node, const char *xpathExpr,
                                         xmlXPathContextPtr xpathCtx) {
  xmlXPathObjectPtr res;
  const char *content;
  res = xmlXPathNodeEval(node, (const xmlChar *)xpathExpr, xpathCtx);
  if (res == NULL || res->nodesetval == NULL) {
    return NULL;
  }
  if (res->nodesetval->nodeNr < 1) {
    return NULL;
  }
  content = (const char *)res->nodesetval->nodeTab[0]->content;
  xmlXPathFreeObject(res);
  return content;
}

// Evaluate an XPath expression and store the result as a float. The
// value pointed by out won't be modified if the XPath expression has
// no result.
static void xpath_get_float(float *out, xmlNodePtr node, const char *xpathExpr,
                            xmlXPathContextPtr xpathCtx) {
  const char *s;

  s = xpath_value_as_string(node, xpathExpr, xpathCtx);
  if (s) {
    *out = strtof(s, NULL);
  }
}

// Evaluate an XPath expression and store the result as an
// integer. The value pointed by out won't be modified if the XPath
// expression has no result.
static void xpath_get_int(int *out, xmlNodePtr node, const char *xpathExpr,
                          xmlXPathContextPtr xpathCtx) {
  const char *s;

  s = xpath_value_as_string(node, xpathExpr, xpathCtx);
  if (s) {
    *out = atoi(s);
  }
}

// Evaluate an XPath expression and store the result as a string. The
// value pointed by out won't be modified if the XPath expression has
// no result. The string will always use newly allocated memory.
static void xpath_get_string(char **out, xmlNodePtr node,
                             const char *xpathExpr,
                             xmlXPathContextPtr xpathCtx) {
  const char *s;

  s = xpath_value_as_string(node, xpathExpr, xpathCtx);
  if (s) {
    *out = strdup(s);
  }
}

// Count the number of nodes returned by an XPath expression.
static int xpath_count(xmlNodePtr node, const char *xpathExpr, xmlXPathContextPtr xpathCtx) {
  xmlXPathObjectPtr res;
  int n;

  res = xmlXPathNodeEval(node, (const xmlChar *)xpathExpr, xpathCtx);
  if (res == NULL || res->nodesetval == NULL) {
    return 0;
  }
  n = res->nodesetval->nodeNr;
  xmlXPathFreeObject(res);
  return n;
}

// Our version of dirname that does not modify the source. Always
// returns newly allocated memory. Assumes that the input does not end
// with a slash.
static char *path_dirname(const char *filename) {
  char *buf = strdup(filename), *p;
  if ((p = strrchr(buf, '/')) != NULL) {
    *p = '\0';
  } else {
    strcpy(buf, ".");
  }
  return buf;
}

// Join two path components. Return a newly allocated string.
static char *path_join(const char *base, const char *filename) {
  char *buf = (char *)malloc(strlen(base) + strlen(filename) + 2);
  sprintf(buf, "%s/%s", base, filename);
  return buf;
}

// Load audio file data into a sample. Will convert the sample rate to
// the desired target using libsamplerate.
static int load_sample_data(struct sample *sample, SNDFILE *sndfile, int target_samplerate) {
  SF_INFO *info = &sample->info;
  float *tmp, *p, *wl, *wr;
  int i;

  tmp = (float *)malloc(sizeof(float) * info->frames * info->channels);
  sf_seek(sndfile, 0, SEEK_SET);
  sf_readf_float(sndfile, tmp, info->frames);

  // Resample the input if necessary, and rewrite the SF_INFO to
  // reflect the updated information.
  if (target_samplerate != info->samplerate) {
    SRC_DATA src_data = {0};
    double ratio;
    int max_out_frames;
    float *resampled;

    ratio = (double)target_samplerate / (double)info->samplerate;
    max_out_frames = (int)(ratio * info->frames) + 32;
    resampled = (float *)malloc(sizeof(float) * max_out_frames * info->channels);
    src_data.data_in = tmp;
    src_data.data_out = resampled;
    src_data.input_frames = info->frames;
    src_data.output_frames = max_out_frames;
    src_data.src_ratio = ratio;
    src_simple(&src_data, SRC_SINC_MEDIUM_QUALITY, info->channels);
    free(tmp);
    tmp = resampled;
    info->samplerate = target_samplerate;
    info->frames = src_data.output_frames_gen;
  }
  
  sample->data_l = (float *)malloc(sizeof(float) * info->frames);
  sample->data_r = (float *)malloc(sizeof(float) * info->frames);

  // Split the file data into separate channels, duplicating if mono.
  for (i = 0, p = tmp, wl = sample->data_l, wr = sample->data_r;
       i < info->frames;
       i++, p += info->channels, wl++, wr++) {
    *wl = *p;
    if (info->channels > 1) {
      *wr = *(p + 1);
    } else {
      *wr = *p;
    }
  }

  free(tmp);

  return 1;
}

// Load an audio file into a sample. Will convert the sample rate to
// the desired target using libsamplerate.
static int load_sample(struct sample *sample, const char *path, int target_samplerate) {
  SF_INFO *info = &sample->info;
  SNDFILE *sndfile;
  int r;

  sndfile = sf_open(path, SFM_READ, info);
  if (!sndfile) {
    fprintf(stderr, "failed to open sample %s: %s\n", path, sf_strerror(NULL));
    return 0;
  }

  // Read the sample data and convert it to the desired format/rate.
  r = load_sample_data(sample, sndfile, target_samplerate);
  sf_close(sndfile);
  if (!r) {
    fprintf(stderr, "failed to load sample data from %s\n", path);
  }
  return r;
}

static void free_sample(struct sample *sample) {
  if (sample->data_l) {
    free(sample->data_l);
  }
  if (sample->data_r) {
    free(sample->data_r);
  }
}

// Parse an instrument layer from an XML document.
static int parse_layer(struct layer *layer, xmlNodePtr node,
                       xmlXPathContextPtr xpathCtx, const char *basedir,
                       int target_samplerate) {
  char *rel_filename = NULL, *filename;
  int r;

  xpath_get_float(&layer->min, node, "dk:min/text()", xpathCtx);
  xpath_get_float(&layer->max, node, "dk:max/text()", xpathCtx);
  xpath_get_float(&layer->gain, node, "dk:gain/text()", xpathCtx);
  xpath_get_string(&rel_filename, node, "dk:filename/text()", xpathCtx);
  if (rel_filename == NULL) {
    return 0;
  }
  filename = path_join(basedir, rel_filename);
  r = load_sample(&layer->sample, filename, target_samplerate);
  free(filename);
  free(rel_filename);
  return r;
}

static void free_layer(struct layer *layer) {
  free_sample(&layer->sample);
}

// Parse an instrument from an XML document.
static int parse_instrument(struct instrument *ins, xmlNodePtr node,
                            xmlXPathContextPtr xpathCtx, const char *basedir,
                            int target_samplerate) {
  const char *instrument_id;
  xmlXPathObjectPtr layers = NULL;
  int i, num_components;

  instrument_id = xpath_value_as_string(node, "dk:id/text()", xpathCtx);
  if (instrument_id == NULL) {
    return 0;
  }

  xpath_get_string(&ins->name, node, "dk:name/text()", xpathCtx);
  xpath_get_float(&ins->pan_l, node, "dk:pan_L/text()", xpathCtx);
  xpath_get_float(&ins->pan_r, node, "dk:pan_R/text()", xpathCtx);
  xpath_get_float(&ins->gain, node, "dk:gain/text()", xpathCtx);
  xpath_get_float(&ins->volume, node, "dk:volume/text()", xpathCtx);
  xpath_get_float(&ins->envelope_attack, node, "dk:Attack/text()", xpathCtx);
  xpath_get_float(&ins->envelope_decay, node, "dk:Decay/text()", xpathCtx);
  xpath_get_float(&ins->envelope_sustain, node, "dk:Sustain/text()", xpathCtx);
  xpath_get_float(&ins->envelope_release, node, "dk:Release/text()", xpathCtx);
  xpath_get_int(&ins->mute_group, node, "dk:muteGroup/text()", xpathCtx);
  xpath_get_int(&ins->note, node, "dk:midiOutNote/text()", xpathCtx);
  
  // Sanity check to spot multi-component instruments.
  num_components = xpath_count(node, ".//dk:instrumentComponent", xpathCtx);
  if (num_components > 1) {
    fprintf(stderr, "found instrument with multiple (%d) components!\n", num_components);
  }

  layers = xmlXPathNodeEval(node, (xmlChar *)".//dk:layer", xpathCtx);
  if (layers == NULL || layers->nodesetval == NULL) {
    fprintf(stderr, "warning: instrument has no layers\n");
    goto fail;
  }

  ins->num_layers = layers->nodesetval->nodeNr;
  if (ins->num_layers > 0) {
    ins->layers = (struct layer *)calloc(ins->num_layers, sizeof(struct layer));
    for (i = 0; i < ins->num_layers; i++) {
      if (!parse_layer(&ins->layers[i], layers->nodesetval->nodeTab[i],
                       xpathCtx, basedir, target_samplerate)) {
        fprintf(stderr, "error parsing instrument layer %d\n", i);
        goto fail;
      }
    }
  } else {
    // Some old drumkit.xml files do not contain layers within
    // instruments but simply specify a filename. In this case we just
    // make up a layer ourselves.
    char *sample_filename = NULL, *full_sample_filename = NULL;
    xpath_get_string(&sample_filename, node, "dk:filename/text()", xpathCtx);
    if (sample_filename) {
      fprintf(stderr, "warning: found legacy (layer-less) instrument\n");

      ins->num_layers = 1;
      ins->layers = (struct layer *)calloc(1, sizeof(struct layer));
      ins->layers[0].min = 0;
      ins->layers[0].max = 1;
      ins->layers[0].gain = 1;
      full_sample_filename = path_join(basedir, sample_filename);
      if (!load_sample(&ins->layers[0].sample, full_sample_filename, target_samplerate)) {
        goto fail;
      }
      free(full_sample_filename);
      free(sample_filename);
    }
  }

  xmlXPathFreeObject(layers);
  return 1;

fail:
  xmlXPathFreeObject(layers);
  return 0;
}

static void free_instrument(struct instrument *ins) {
  int i;
  for (i = 0; i < ins->num_layers; i++) {
    free_layer(&ins->layers[i]);
  }
  if (ins->layers)
    free(ins->layers);
  if (ins->name)
    free(ins->name);
}

// Find the instrument layer corresponding to a specific velocity.
struct layer *instrument_find_layer_by_velocity(struct instrument *ins, float velocity) {
  int i;

  for (i = 0; i < ins->num_layers; i++) {
    if (velocity >= ins->layers[i].min && velocity <= ins->layers[i].max) {
      return &ins->layers[i];
    }
  }
  return &ins->layers[ins->num_layers - 1];
}

// Parse a list of instruments from an XML document.
static int parse_instruments(struct instrument **instruments,
                             xmlNodeSetPtr nodes, xmlXPathContextPtr xpathCtx,
                             const char *basedir, int target_samplerate) {
  int i, n;

  n = nodes->nodeNr;
  *instruments = (struct instrument *)calloc(n, sizeof(struct instrument));

  for (i = 0; i < n; i++) {
    instrument_set_defaults(&(*instruments)[i]);
    (*instruments)[i].index = i;
    if (!parse_instrument(&(*instruments)[i], nodes->nodeTab[i], xpathCtx, basedir, target_samplerate)) {
      return 0;
    }
  }
  return n;
}

static void free_instruments(struct instrument *instruments, int n) {
  int i;
  for (i = 0; i < n; i++) {
    free_instrument(&instruments[i]);
  }
}

// Some drumkits lack an explicit MIDI note assignment: these are
// recognizable because they set all instruments to the same MIDI
// note. We can detect this situation and fix it with a default note
// assignment.
static void fix_midi_notes(struct drumkit *kit) {
  int note_count[127] = {0};
  int note_cardinality = 0;
  int i;

  for (i = 0; i < kit->num_instruments; i++) {
    note_count[kit->instruments[i].note]++;
  }
  for (i = 0; i < 127; i++) {
    if (note_count[i] > 0) {
      note_cardinality++;
    }
  }

  // Assign default MIDI note mapping if all the notes are the same.
  if (note_cardinality == 1) {
    fprintf(stderr, "warning: drumkit lacks proper MIDI note mapping, setting default assignments\n");
    for (i = 0; i < kit->num_instruments; i++) {
      kit->instruments[i].note = i + 36;
    }
  }
}

struct drumkit *drumkit_new(const char *filename, int target_samplerate) {
  struct drumkit *kit = (struct drumkit *)calloc(1, sizeof(struct drumkit));
  if (!drumkit_load(kit, filename, target_samplerate)) {
    free(kit);
    return NULL;
  }
  return kit;
}

// Assign the Hydrogen drumkit namespace to all XML nodes in a document.
//
// BUG: this causes a memory leak of a few bytes per XML node,
// reported by the address sanitizer.
static void fix_bad_xml(xmlNodePtr cur, xmlNsPtr ns) {
  for (; cur; cur = cur->next) {
    if (cur->type == XML_ELEMENT_NODE && !cur->ns) {
      xmlSetNs(cur, ns);
    }
    fix_bad_xml(cur->children, ns);
  }
}

// Return the final location pointed at by filename if it's a symbolic
// link. In all cases, the resulting memory must be freed by the caller.
static char *resolve_symlink(const char *filename) {
  char buf[1024];
  struct stat stbuf;
  char *cur = strdup(filename);
  int n;

  while (1) {
    if (lstat(cur, &stbuf) < 0) {
      return NULL;
    }
    if ((stbuf.st_mode & S_IFLNK) != S_IFLNK) {
      return cur;
    }

    n = readlink(cur, buf, sizeof(buf)-1);
    if (n < 0) {
      return NULL;
    }
    buf[n] = '\0';
    free(cur);
    cur = strdup(buf);
  }
}

int drumkit_load(struct drumkit *kit, const char *filename, int target_samplerate) {
  char *basedir = NULL, *resolved_filename = NULL;
  xmlDocPtr doc = NULL;
  xmlNsPtr ns = NULL;
  xmlNodePtr root_element = NULL;
  xmlXPathContextPtr xpathCtx = NULL;
  xmlXPathObjectPtr xpathObj = NULL;
  int n, r = 1;

  // Resolve symlinks because sometimes (i.e. in presets) we get sent
  // a symbolic link to the actual drumkit.xml, which messes up the
  // relative filenames of the samples.
  resolved_filename = resolve_symlink(filename);
  basedir = path_dirname(resolved_filename);
  kit->instruments = NULL;

  doc = xmlParseFile(filename);
  if (doc == NULL) {
    fprintf(stderr, "Error in xmlParseFile\n");
    r = 0;
    goto cleanup;
  }

  // Check for malformed XML (missing namespace). We can convince
  // libxml2 to parse it anyway by setting the proper namespace on all
  // elements.
  root_element = xmlDocGetRootElement(doc);
  if (!strcmp((const char *)root_element->name, "drumkit_info") && root_element->ns == NULL) {
    fprintf(stderr, "spotted old-style (bad) XML\n");
    ns = xmlNewNs(root_element, (xmlChar *)"http://www.hydrogen-music.org/drumkit", (xmlChar *)"");
    fix_bad_xml(root_element, ns);
  }

  xpathCtx = xmlXPathNewContext(doc);
  if (xpathCtx == NULL) {
    fprintf(stderr, "Error: unable to create new XPath context\n");
    r = 0;
    goto cleanup;
  }
  xmlXPathRegisterNs(xpathCtx, (xmlChar *)"dk",
                     (xmlChar *)"http://www.hydrogen-music.org/drumkit");

  xpathObj = xmlXPathEvalExpression(
      (xmlChar *)"//dk:instrumentList/dk:instrument", xpathCtx);
  if (xpathObj == NULL || xpathObj->nodesetval == NULL) {
    fprintf(stderr,
            "Error: unable to evaluate xpath expression or empty result\n");
    r = 0;
    goto cleanup;
  }

  n = parse_instruments(&kit->instruments, xpathObj->nodesetval, xpathCtx, basedir, target_samplerate);
  if (n <= 0) {
    r = 0;
  } else {
    kit->num_instruments = n;
  }

  kit->path = strdup(resolved_filename);
  fix_midi_notes(kit);

 cleanup:
  free(resolved_filename);
  free(basedir);
  if (xpathObj) {
    xmlXPathFreeObject(xpathObj);
  }
  if (xpathCtx) {
    xmlXPathFreeContext(xpathCtx);
  }
  if (doc) {
    xmlFreeDoc(doc);
  }
  return r;
}

void drumkit_free(struct drumkit *kit) {
  if (kit->num_instruments > 0) {
    free_instruments(kit->instruments, kit->num_instruments);
    free(kit->instruments);
  }
  free(kit->path);
}

void drumkit_print(struct drumkit *kit) {
  int i;
  for (i = 0; i < kit->num_instruments; i++) {
    print_instrument(&kit->instruments[i]);
  }
}

struct instrument *drumkit_find_instrument_by_note(struct drumkit *kit, int note) {
  int i;
  for (i = 0; i < kit->num_instruments; i++) {
    if (kit->instruments[i].note == note) {
      return &kit->instruments[i];
    }
  }
  return NULL;
}

#include "config.h"

#include <stdio.h>

#include "voice.h"

static float velocity_to_gain(int velocity) {
  float f = (float)velocity / 128.0;
  return f * f;
}

static float volume_to_gain(int volume) {
  return volume * volume;
}

void voice_init(struct voice *voice, int samplerate) {
  voice->samplerate = samplerate;
  voice->cur_sample = NULL;
  voice->playing = 0;
  voice->in_preamble = 0;
}

int voice_noteon(struct voice *voice, struct instrument *ins, int note, int velocity, int preamble) {
  struct layer *layer;

  layer = instrument_find_layer_by_velocity(ins, velocity);
  if (layer == NULL) {
    return 0;
  }

  voice->cur_sample = &layer->sample;
  voice->pan_l = ins->pan_l;
  voice->pan_r = ins->pan_r;
  voice->gain = volume_to_gain(ins->volume) * ins->gain * layer->gain * velocity_to_gain(velocity);
  voice->mute_group = ins->mute_group;
  adsr_trigger(&voice->adsr,
               (int)ins->envelope_attack,
               (int)ins->envelope_decay,
               ins->envelope_sustain,
               (int)ins->envelope_release);
  
  voice->note = note;
  if (preamble > 0) {
    voice->in_preamble = 1;
    voice->preamble_countdown = preamble;
  } else {
    voice->in_preamble = 0;
    voice->playing = 1;
    voice->ticks = 0;
  }    

  return 1;
}

int voice_noteoff_if_note(struct voice *voice, int note) {
  if ((voice->playing || voice->in_preamble) && voice->note == note) {
    adsr_release(&voice->adsr);
    return 1;
  }
  return 0;
}

int voice_noteoff_if_mute_group(struct voice *voice, int mute_group) {
  if ((voice->playing || voice->in_preamble) && voice->mute_group == mute_group) {
    adsr_release(&voice->adsr);
    return 1;
  }
  return 0;
}

void voice_process(struct voice *voice, float *out_l, float *out_r, int num_frames) {
  // Accumulate the sample to the output buffer.
  while (num_frames--) {
    if (voice->in_preamble) {
      voice->preamble_countdown--;
      if (voice->preamble_countdown <= 0) {
        voice->in_preamble = 0;
        voice->playing = 1;
        voice->ticks = 0;
      }
    }

    if (voice->playing) {
      float gain = voice->gain * adsr_get_value(&voice->adsr, 1);

      // Stop playing when the envelope is done, or the sample is
      // done, whichever comes first.
      if ((voice->adsr.state == ADSR_IDLE)
          || (voice->ticks >= voice->cur_sample->info.frames)) {
        voice->playing = 0;
        continue;
      }

      *out_l++ += voice->pan_l * gain * voice->cur_sample->data_l[voice->ticks];
      *out_r++ += voice->pan_r * gain * voice->cur_sample->data_r[voice->ticks];

      voice->ticks++;
    } else {
      out_l++;
      out_r++;
    }
  }
}

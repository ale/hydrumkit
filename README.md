hydrumkit
===

HyDrumkit is a LV2 drum sampler plugin. It can load
[Hydrogen](http://www.hydrogen-music.com/) drum kits and play them,
with minimal runtime controls. It will use settings (volume, pan,
envelopes, etc.) from the drumkit itself, and downmix the instruments
to a simple stereo output.

Some features:

* full LV2 support
  * state save/restore
  * asynchronous loading of samples
* extensive support for Hydrogen drumkit features
  * multi-layer instruments
  * pan / volume
  * envelopes
  * notably missing: pitch shifting
* minimalist UI with a single button to load a new drumkit
* a simple *humanizer* (modeled after the one in
  [Drumgizmo](https://drumgizmo.org/))

It offers very similar functionality to
[DrMr](https://github.com/nicklan/drmr), but with a slightly improved
engine and fewer controls: the idea is that you set up and tune the
drumkit in Hydrogen itself, and only use *hydrumkit* to play it in
your DAW.

The code is based on the [LV2 Sampler
example](http://lv2plug.in/book/#_sampler), with abundant inspiration
taken from [Fabla](https://github.com/openAVproductions/openAV-Fabla),
[Drumgizmo](https://drumgizmo.org/) and of course Hydrogen itself.

## Installation

To install from source, run:

```shell
autoreconf --install
./configure
make
make install
```

By default this will install the plugin in *~/.lv2/*, but this can be
controlled with the *--with-lv2-plugin-dir* option to *configure*.

#include <stdio.h>

#include <libxml/parser.h>

#include "drumkit.h"

int main(int argc, char **argv) {
  int status = 0;
  struct drumkit kit = {0};

  if (argc != 2) {
    return 2;
  }

  /*
   * this initialize the library and check potential ABI mismatches
   * between the version it was compiled for and the actual shared
   * library used.
   */
  LIBXML_TEST_VERSION

  if (!drumkit_load(&kit, argv[1], 96000)) {
    status = 1;
  } else {
    drumkit_print(&kit);
  }

  drumkit_free(&kit);

  /*
   * Cleanup function for the XML library.
   */
  xmlCleanupParser();

  return status;
}

#include <stdio.h>
#include <libxml/parser.h>
#include <sndfile.h>

#include "drumkit.h"
#include "voice.h"

#define SR 96000

void write_sample(const char *filename, float *data_l, float *data_r, int num_frames) {
  SF_INFO info = {0};
  SNDFILE *sndfile;
  float *buf;
  int i;

  buf = (float *)malloc(2 * sizeof(float) * num_frames);
  for (i = 0; i < num_frames; i++) {
    buf[i*2] = data_l[i];
    buf[i*2+1] = data_r[i];
  }

  info.samplerate = SR;
  info.channels = 2;
  info.format = SF_FORMAT_WAV | SF_FORMAT_FLOAT;
  
  sndfile = sf_open(filename, SFM_WRITE, &info);
  sf_writef_float(sndfile, buf, num_frames);
  sf_close(sndfile);
  free(buf);
}

int main(int argc, char **argv) {
  int status = 0;
  struct drumkit kit = {0};
  struct voice voice;
  struct instrument *ins;
  struct layer *layer;
  float *out_l = NULL, *out_r = NULL;
  int total_samples;
  int lead_samples = 100;
  int note_samples = 100000;
  int note = 36;
  int velocity = 100;
  int i;

  if (argc != 2) {
    return 2;
  }

  /*
   * this initialize the library and check potential ABI mismatches
   * between the version it was compiled for and the actual shared
   * library used.
   */
  LIBXML_TEST_VERSION

  if (!drumkit_load(&kit, argv[1], SR)) {
    status = 1;
    goto cleanup;
  }

  ins = drumkit_find_instrument_by_note(&kit, note);
  if (!ins) {
    printf("no instrument for note %d\n", note);
    status = 1;
    goto cleanup;
  }

  voice_init(&voice, SR);
  total_samples = lead_samples + note_samples;
  out_l = (float *)malloc(sizeof(float) * total_samples);
  out_r = (float *)malloc(sizeof(float) * total_samples);
  for (i = 0; i < total_samples; i++) {
    out_l[i] = out_r[i] = 0;
  }

  // Some silence first.
  voice_process(&voice, out_l, out_r, lead_samples);
  // Then a note (kick).
  voice_noteon(&voice, ins, note, velocity, 0);
  voice_process(&voice, out_l + lead_samples, out_r + lead_samples, note_samples);

  // Dump the layer used by the voice.
  layer = instrument_find_layer_by_velocity(ins, velocity);
  write_sample("layer.wav", layer->sample.data_l, layer->sample.data_r, layer->sample.info.frames);
  
  // Dump the output.
  write_sample("out.wav", out_l, out_r, total_samples);
  
 cleanup:
  if (out_l)
    free(out_l);
  if (out_r)
    free(out_r);
  drumkit_free(&kit);

  /*
   * Cleanup function for the XML library.
   */
  xmlCleanupParser();

  return status;
}


#include "lv2/lv2plug.in/ns/ext/atom/atom.h"
#include "lv2/lv2plug.in/ns/ext/atom/forge.h"
#include "lv2/lv2plug.in/ns/ext/atom/util.h"
#include "lv2/lv2plug.in/ns/ext/log/log.h"
#include "lv2/lv2plug.in/ns/ext/log/logger.h"
#include "lv2/lv2plug.in/ns/ext/midi/midi.h"
#include "lv2/lv2plug.in/ns/extensions/ui/ui.h"
#include "lv2/lv2plug.in/ns/ext/urid/urid.h"
#include "lv2/lv2plug.in/ns/lv2core/lv2.h"

#if HAVE_LV2_LV2PLUG_IN_NS_LV2CORE_LV2_UTIL_H
#include "lv2/lv2plug.in/ns/ext/core/lv2_util.h"
#else
#include "lv2_util.h"
#endif

#include <gdk/gdk.h>
#include <glib-object.h>
#include <glib.h>
#include <gobject/gclosure.h>
#include <gtk/gtk.h>
#include "plugin/widgets/knob.h"

#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

#include "ports.h"
#include "uris.h"

#define KNOB_SIZE 32

struct plugin_ui {
  LV2_Atom_Forge forge;
  LV2_URID_Map *map;
  LV2_Log_Logger logger;
  plugin_uris uris;

  LV2UI_Write_Function write;
  LV2UI_Controller controller;

  GtkWidget *label;
  GtkWidget *load_button;
  GtkWidget *load_dialog;
  GtkWidget *button_box;
  GtkWidget *box;
  GtkWidget *frame;
  GtkWidget *humanize_frame;
  GtkWidget *humanize_box;
  GtkWidget *humanize_button;
  GtkWidget *humanize_latency_max_knob;
  GtkWidget *humanize_latency_stddev_knob;
  GtkWidget *humanize_latency_laid_back_knob;
  GtkWidget *humanize_latency_regain_knob;
  GtkWidget *humanize_velocity_stddev_knob;
  GtkWidget *window; /* For optional show interface. */

  int humanize_enabled;
  float humanize_latency_max_value;
  float humanize_latency_stddev_value;
  float humanize_latency_laid_back_value;
  float humanize_latency_regain_value;
  float humanize_velocity_stddev_value;

  char *filename;

  uint8_t forge_buf[1024];
};

static void on_load_button_click(GtkButton *widget, void *handle) {
  struct plugin_ui *ui = (struct plugin_ui *)handle;

  /* Setup the dialog parent to be chooser button's toplevel, and be modal
     as needed. */
  if (!gtk_widget_get_visible(ui->load_dialog)) {
    GtkWidget *toplevel;

    toplevel = gtk_widget_get_toplevel(GTK_WIDGET(widget));
    if (gtk_widget_is_toplevel(toplevel) && GTK_IS_WINDOW(toplevel)) {
      if (GTK_WINDOW(toplevel) != gtk_window_get_transient_for(GTK_WINDOW(ui->load_dialog)))
        gtk_window_set_transient_for(GTK_WINDOW(ui->load_dialog), GTK_WINDOW(toplevel));

      gtk_window_set_modal(GTK_WINDOW(ui->load_dialog),
                           gtk_window_get_modal(GTK_WINDOW(toplevel)));
    }
  }

  gtk_window_present(GTK_WINDOW(ui->load_dialog));
}

static void on_load_dialog_response(GtkDialog *widget, gint response, void *handle) {
  struct plugin_ui *ui = (struct plugin_ui *)handle;

  gtk_widget_hide(ui->load_dialog);
  
  if (response == GTK_RESPONSE_ACCEPT || response == GTK_RESPONSE_OK) {
    LV2_Atom *msg;

    // Get the filename from the file chooser
    char *filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(ui->load_dialog));

    // Write a set message to the plugin to load new file
    lv2_atom_forge_set_buffer(&ui->forge, ui->forge_buf, sizeof(ui->forge_buf));
    msg = (LV2_Atom *)write_set_file(&ui->forge, &ui->uris, filename,
                                     strlen(filename));

    ui->write(ui->controller, PORT_CONTROL, lv2_atom_total_size(msg),
              ui->uris.atom_eventTransfer, msg);

    g_free(filename);
  };
}

#define WRITECONTROLVALUE(expr, port) {                                     \
    float val_ = (expr);                                                    \
    ui->write(ui->controller, port, sizeof(float), 0, (const void *)&val_); \
  }

static void write_control_values(struct plugin_ui *ui) {
  // Values here should match the ranges specified in hydrumkit.ttl.
  WRITECONTROLVALUE(ui->humanize_latency_max_value * 100.0, PORT_HUMANIZE_LATENCY_MAX);
  WRITECONTROLVALUE(ui->humanize_latency_stddev_value * 20.0, PORT_HUMANIZE_LATENCY_STDDEV);
  WRITECONTROLVALUE(ui->humanize_latency_laid_back_value * 100.0, PORT_HUMANIZE_LATENCY_LAID_BACK);
  WRITECONTROLVALUE(ui->humanize_latency_regain_value, PORT_HUMANIZE_LATENCY_REGAIN);
  WRITECONTROLVALUE(ui->humanize_velocity_stddev_value * 0.3, PORT_HUMANIZE_VELOCITY_STDDEV);
}

static void on_humanize_button_click(GtkButton *widget, void *handle) {
  struct plugin_ui *ui = (struct plugin_ui *)handle;

  ui->humanize_enabled = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(widget));
  WRITECONTROLVALUE((float)ui->humanize_enabled, PORT_HUMANIZE_ENABLE);
}

static void on_humanize_knob_change(KnobWidget *widget, void *handle) {
  struct plugin_ui *ui = (struct plugin_ui *)handle;
  write_control_values(ui);
}

static int path_exists(const char *path) {
  struct stat stbuf;
  return (0 == stat(path, &stbuf)) ? 1 : 0;
}

static LV2UI_Handle instantiate(const LV2UI_Descriptor *descriptor,
                                const char *plugin_uri, const char *bundle_path,
                                LV2UI_Write_Function write_function,
                                LV2UI_Controller controller,
                                LV2UI_Widget *widget,
                                const LV2_Feature *const *features) {
  struct plugin_ui *ui =
      (struct plugin_ui *)calloc(1, sizeof(struct plugin_ui));
  const char *missing;
  char *home;
  LV2_Atom *msg;
  LV2_Atom_Forge_Frame frame;
  GtkFileFilter *filter;

  if (!ui) {
    return NULL;
  }

  ui->write = write_function;
  ui->controller = controller;
  *widget = NULL;

  // Get host features
  missing = lv2_features_query(features, LV2_LOG__log, &ui->logger.log, false,
                               LV2_URID__map, &ui->map, true, NULL);
  lv2_log_logger_set_map(&ui->logger, ui->map);
  if (missing) {
    lv2_log_error(&ui->logger, "Missing feature <%s>\n", missing);
    free(ui);
    return NULL;
  }

  // Map URIs and initialise forge
  map_sampler_uris(ui->map, &ui->uris);
  lv2_atom_forge_init(&ui->forge, ui->map);

  // Construct Gtk UI
  ui->box = gtk_grid_new();
  gtk_orientable_set_orientation(GTK_ORIENTABLE(ui->box), GTK_ORIENTATION_VERTICAL);
  ui->frame = gtk_frame_new("Drumkit");
  ui->label = gtk_label_new("No drumkit loaded.");
  ui->button_box = gtk_grid_new();
  ui->humanize_frame = gtk_frame_new("Humanizer");
  ui->humanize_box = gtk_grid_new();

  // Create the file chooser (we use a custom GtkButton instead of the
  // default GtkFileChooserButton because the latter takes way too
  // much UI real estate).
  filter = gtk_file_filter_new();
  gtk_file_filter_add_pattern(filter, "drumkit.xml");

  ui->load_button = gtk_button_new_from_icon_name("document-open", GTK_ICON_SIZE_BUTTON);
  g_signal_connect(ui->load_button, "clicked", G_CALLBACK(on_load_button_click), ui);

  ui->load_dialog = gtk_file_chooser_dialog_new(NULL, NULL,
                                                GTK_FILE_CHOOSER_ACTION_OPEN,
                                                "_Cancel", GTK_RESPONSE_CANCEL,
                                                "_Open", GTK_RESPONSE_ACCEPT,
                                                NULL);
  gtk_file_chooser_set_filter(GTK_FILE_CHOOSER(ui->load_dialog), filter);

  // Add common paths for system and user Hydrogen drumkit installs.
  if (path_exists("/usr/share/hydrogen/data/drumkits")) {
    gtk_file_chooser_add_shortcut_folder(GTK_FILE_CHOOSER(ui->load_dialog), "/usr/share/hydrogen/data/drumkits", NULL);
  }
  if (path_exists("/usr/local/share/hydrogen/data/drumkits")) {
    gtk_file_chooser_add_shortcut_folder(GTK_FILE_CHOOSER(ui->load_dialog), "/usr/local/share/hydrogen/data/drumkits", NULL);
  }
  home = getenv("HOME");
  if (home) {
    char *homebuf = (char *)malloc(strlen(home) + 48);
    sprintf(homebuf, "%s/.hydrogen/data/drumkits", home);
    if (path_exists(homebuf)) {
      gtk_file_chooser_add_shortcut_folder(GTK_FILE_CHOOSER(ui->load_dialog), homebuf, NULL);
    }
    free(homebuf);
  }

  // Create the humanizer controls.
  ui->humanize_button = gtk_check_button_new_with_label("Enable");
  ui->humanize_latency_max_knob = knob_widget_new(&ui->humanize_latency_max_value, KNOB_SIZE, 0);
  ui->humanize_latency_stddev_knob = knob_widget_new(&ui->humanize_latency_stddev_value, KNOB_SIZE, 0);
  ui->humanize_latency_laid_back_knob = knob_widget_new(&ui->humanize_latency_laid_back_value, KNOB_SIZE, 0);
  ui->humanize_latency_regain_knob = knob_widget_new(&ui->humanize_latency_regain_value, KNOB_SIZE, 0);
  ui->humanize_velocity_stddev_knob = knob_widget_new(&ui->humanize_velocity_stddev_value, KNOB_SIZE, 0);
  g_signal_connect(ui->humanize_button, "clicked", G_CALLBACK(on_humanize_button_click), ui);
  g_signal_connect(ui->humanize_latency_max_knob, "value-changed", G_CALLBACK(on_humanize_knob_change), ui);
  g_signal_connect(ui->humanize_latency_stddev_knob, "value-changed", G_CALLBACK(on_humanize_knob_change), ui);
  g_signal_connect(ui->humanize_latency_laid_back_knob, "value-changed", G_CALLBACK(on_humanize_knob_change), ui);
  g_signal_connect(ui->humanize_latency_regain_knob, "value-changed", G_CALLBACK(on_humanize_knob_change), ui);
  g_signal_connect(ui->humanize_velocity_stddev_knob, "value-changed", G_CALLBACK(on_humanize_knob_change), ui);

  // Place all elements inside their containers.
  gtk_container_set_border_width(GTK_CONTAINER(ui->box), 4);
  gtk_container_add(GTK_CONTAINER(ui->button_box), ui->label);
  gtk_widget_set_hexpand(ui->label, TRUE);
  gtk_widget_set_halign(ui->label, GTK_ALIGN_CENTER);
  gtk_container_add(GTK_CONTAINER(ui->button_box), ui->load_button);
  gtk_widget_set_hexpand(ui->load_button, FALSE);
  gtk_container_add(GTK_CONTAINER(ui->frame), ui->button_box);
  gtk_container_add(GTK_CONTAINER(ui->box), ui->frame);
  gtk_container_add(GTK_CONTAINER(ui->humanize_box), ui->humanize_button);
  gtk_container_add(GTK_CONTAINER(ui->humanize_box), ui->humanize_latency_max_knob);
  gtk_container_add(GTK_CONTAINER(ui->humanize_box), ui->humanize_latency_stddev_knob);
  gtk_container_add(GTK_CONTAINER(ui->humanize_box), ui->humanize_latency_laid_back_knob);
  gtk_container_add(GTK_CONTAINER(ui->humanize_box), ui->humanize_latency_regain_knob);
  gtk_container_add(GTK_CONTAINER(ui->humanize_box), ui->humanize_velocity_stddev_knob);
  gtk_container_add(GTK_CONTAINER(ui->humanize_frame), ui->humanize_box);
  gtk_container_add(GTK_CONTAINER(ui->box), ui->humanize_frame);

  g_signal_connect(ui->load_dialog, "response", G_CALLBACK(on_load_dialog_response), ui);

  // Request state (filename) from plugin
  lv2_atom_forge_set_buffer(&ui->forge, ui->forge_buf, sizeof(ui->forge_buf));
  msg = (LV2_Atom *)lv2_atom_forge_object(&ui->forge, &frame, 0,
                                          ui->uris.patch_Get);
  lv2_atom_forge_pop(&ui->forge, &frame);

  ui->write(ui->controller, 0, lv2_atom_total_size(msg),
            ui->uris.atom_eventTransfer, msg);

  *widget = ui->box;

  return ui;
}

static void cleanup(LV2UI_Handle handle) {
  struct plugin_ui *ui = (struct plugin_ui *)handle;

  gtk_widget_destroy(ui->label);
  gtk_widget_destroy(ui->load_dialog);
  gtk_widget_destroy(ui->load_button);
  gtk_widget_destroy(ui->button_box);
  gtk_widget_destroy(ui->frame);
  gtk_widget_destroy(ui->humanize_button);
  gtk_widget_destroy(ui->humanize_latency_max_knob);
  gtk_widget_destroy(ui->humanize_latency_stddev_knob);
  gtk_widget_destroy(ui->humanize_latency_laid_back_knob);
  gtk_widget_destroy(ui->humanize_latency_regain_knob);
  gtk_widget_destroy(ui->humanize_velocity_stddev_knob);
  gtk_widget_destroy(ui->humanize_box);
  gtk_widget_destroy(ui->humanize_frame);

  gtk_widget_destroy(ui->box);
  free(ui);
}

// The drumkit "name" that we show to the user is just the basename of
// the drumkit directory.
static char *get_drumkit_label(const char *filename) {
  char *buf = g_strdup(filename), *p, *q, *result;

  p = strrchr(buf, '/');
  q = p;
  do {
    q--;
  } while (q > buf && *q != '/');
  *p = '\0';
  result = g_strdup(q + 1);
  free(buf);
  return result;
}

static void port_event(LV2UI_Handle handle, uint32_t port_index,
                       uint32_t buffer_size, uint32_t format,
                       const void *buffer) {
  struct plugin_ui *ui = (struct plugin_ui *)handle;

  if (format == ui->uris.atom_eventTransfer && port_index == PORT_NOTIFY) {
    const LV2_Atom *atom = (const LV2_Atom *)buffer;
    if (lv2_atom_forge_is_object_type(&ui->forge, atom->type)) {
      const LV2_Atom_Object *obj = (const LV2_Atom_Object *)atom;
      if (obj->body.otype == ui->uris.patch_Set) {
        const char *path = read_set_file(&ui->uris, obj);
        if (path && (!ui->filename || strcmp(path, ui->filename))) {
          char *label = get_drumkit_label(path);

          g_free(ui->filename);
          ui->filename = g_strdup(path);
          gtk_file_chooser_set_filename(GTK_FILE_CHOOSER(ui->load_dialog),
                                        path);
          gtk_label_set_text(GTK_LABEL(ui->label), label);
        } else if (!path) {
          lv2_log_warning(&ui->logger, "Set message has no path\n");
        }
      }
    } else {
      lv2_log_error(&ui->logger, "Unknown message type\n");
    }
  } else if (port_index == PORT_HUMANIZE_LATENCY_MAX) {
    float val = *(float *)buffer;
    ui->humanize_latency_max_value = val / 400.0;
    gtk_widget_queue_draw(ui->humanize_latency_max_knob);
  } else if (port_index == PORT_HUMANIZE_LATENCY_STDDEV) {
    float val = *(float *)buffer;
    ui->humanize_latency_stddev_value = val / 100.0;
    gtk_widget_queue_draw(ui->humanize_latency_stddev_knob);
  } else if (port_index == PORT_HUMANIZE_LATENCY_LAID_BACK) {
    float val = *(float *)buffer;
    ui->humanize_latency_laid_back_value = val / 100.0;
    gtk_widget_queue_draw(ui->humanize_latency_laid_back_knob);
  } else if (port_index == PORT_HUMANIZE_LATENCY_REGAIN) {
    float val = *(float *)buffer;
    ui->humanize_latency_max_value = val;
    gtk_widget_queue_draw(ui->humanize_latency_max_knob);
  } else if (port_index == PORT_HUMANIZE_VELOCITY_STDDEV) {
    float val = *(float *)buffer;
    ui->humanize_velocity_stddev_value = val;
    gtk_widget_queue_draw(ui->humanize_velocity_stddev_knob);
  } else if (port_index == PORT_HUMANIZE_ENABLE) {
    float val = *(float *)buffer;
    int enabled = (val > 0) ? 1 : 0;
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(ui->humanize_button), enabled);
  } else {
    lv2_log_warning(&ui->logger, "Unknown port event format\n");
  }
}

/* Optional non-embedded UI show interface. */
static int ui_show(LV2UI_Handle handle) {
  struct plugin_ui *ui = (struct plugin_ui *)handle;
  int argc = 0;

  gtk_init(&argc, NULL);

  ui->window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_container_add(GTK_CONTAINER(ui->window), ui->box);
  gtk_widget_show_all(ui->window);
  gtk_window_present(GTK_WINDOW(ui->window));

  return 0;
}

/* Optional non-embedded UI hide interface. */
static int ui_hide(LV2UI_Handle handle) { return 0; }

/* Idle interface for optional non-embedded UI. */
static int ui_idle(LV2UI_Handle handle) {
  struct plugin_ui *ui = (struct plugin_ui *)handle;
  if (ui->window) {
    gtk_main_iteration();
  }
  return 0;
}

static const void *extension_data(const char *uri) {
  static const LV2UI_Show_Interface show = {ui_show, ui_hide};
  static const LV2UI_Idle_Interface idle = {ui_idle};

  if (!strcmp(uri, LV2_UI__showInterface)) {
    return &show;
  } else if (!strcmp(uri, LV2_UI__idleInterface)) {
    return &idle;
  }
  return NULL;
}

static const LV2UI_Descriptor descriptor = {
  EG_SAMPLER_UI_URI,
  instantiate,
  cleanup,
  port_event,
  extension_data
};

LV2_SYMBOL_EXPORT
const LV2UI_Descriptor *lv2ui_descriptor(uint32_t index) {
  switch (index) {
  case 0:
    return &descriptor;
  default:
    return NULL;
  }
}

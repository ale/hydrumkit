
#include "lv2/lv2plug.in/ns/ext/atom/atom.h"
#include "lv2/lv2plug.in/ns/ext/atom/forge.h"
#include "lv2/lv2plug.in/ns/ext/atom/util.h"
#include "lv2/lv2plug.in/ns/lv2core/lv2.h"
#include "lv2/lv2plug.in/ns/ext/log/log.h"
#include "lv2/lv2plug.in/ns/ext/log/logger.h"
#include "lv2/lv2plug.in/ns/ext/midi/midi.h"
#include "lv2/lv2plug.in/ns/ext/state/state.h"
#include "lv2/lv2plug.in/ns/ext/urid/urid.h"
#include "lv2/lv2plug.in/ns/ext/worker/worker.h"

#if HAVE_LV2_LV2PLUG_IN_NS_LV2CORE_LV2_UTIL_H
#include "lv2/lv2plug.in/ns/ext/core/lv2_util.h"
#else
#include "lv2_util.h"
#endif

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

#include "engine/sampler.h"
#include "atom_sink.h"
#include "ports.h"
#include "uris.h"

#define NUM_VOICES 64

struct sampler_plugin {
  LV2_URID_Map *map;
  LV2_Worker_Schedule *schedule;
  LV2_Log_Logger logger;

  const LV2_Atom_Sequence *control_port;
  LV2_Atom_Sequence *notify_port;
  float *output_port_l;
  float *output_port_r;
  float *output_latency;
  float *input_ports[PORT_LAST];

  LV2_Atom_Forge_Frame notify_frame;
  LV2_Atom_Forge forge;

  plugin_uris uris;

  struct humanizer_settings hsettings;
  struct sampler *sampler;

  int activated;
  int send_settings_to_ui;
  uint32_t frame_offset;
  int samplerate;
};

static LV2_Worker_Status work(LV2_Handle instance,
                              LV2_Worker_Respond_Function respond,
                              LV2_Worker_Respond_Handle handle, uint32_t size,
                              const void *data) {
  struct sampler_plugin *plugin = (struct sampler_plugin *)instance;
  const LV2_Atom *atom = (const LV2_Atom *)data;

  if (atom->type == plugin->uris.eg_freeDrumkit) {
    // Free old drumkit.
    struct drumkit **new_kit = (struct drumkit **)data;
    drumkit_free(*new_kit);
    free(*new_kit);
  } else if (atom->type == plugin->forge.Object) {
    const LV2_Atom_Object *obj = (const LV2_Atom_Object *)data;
    const char *path = read_set_file(&plugin->uris, obj);
    struct drumkit *new_kit;

    if (!path) {
      lv2_log_error(&plugin->logger, "Malformed set drumkit request\n");
      return LV2_WORKER_ERR_UNKNOWN;
    }

    // Attempt to load the drumkit from path.
    new_kit = drumkit_new(path, plugin->samplerate);
    if (!new_kit) {
      lv2_log_error(&plugin->logger, "Error loading the drumkit %s\n", path);
      return LV2_WORKER_ERR_UNKNOWN;
    }

    // Respond with pointer.
    respond(handle, sizeof(struct drumkit *), &new_kit);
  }

  return LV2_WORKER_SUCCESS;
}

static LV2_Worker_Status work_response(LV2_Handle instance, uint32_t size,
                                       const void *data) {
  struct sampler_plugin *plugin = (struct sampler_plugin *)instance;
  struct drumkit *new_kit = *(struct drumkit **)data;
  struct drumkit *old_kit = plugin->sampler->kit;

  sampler_set_drumkit(plugin->sampler, new_kit);

  // Free the old drumkit in the worker.
  if (old_kit) {
    plugin->schedule->schedule_work(plugin->schedule->handle,
                                    sizeof(struct drumkit *), &old_kit);
  }

  // Send UI notification that we're using the new drumkit.
  lv2_atom_forge_frame_time(&plugin->forge, plugin->frame_offset);
  write_set_file(&plugin->forge, &plugin->uris,
                 plugin->sampler->kit->path,
                 strlen(plugin->sampler->kit->path));

  return LV2_WORKER_SUCCESS;
}

static void activate(LV2_Handle instance) {
  struct sampler_plugin *plugin = (struct sampler_plugin *)instance;
  plugin->activated = 1;
}

static void deactivate(LV2_Handle instance) {
  struct sampler_plugin *plugin = (struct sampler_plugin *)instance;
  plugin->activated = 0;
}

static void connect_port(LV2_Handle instance, uint32_t port, void *data) {
  struct sampler_plugin *plugin = (struct sampler_plugin *)instance;
  switch (port) {
  case PORT_CONTROL:
    plugin->control_port = (const LV2_Atom_Sequence *)data;
    break;
  case PORT_NOTIFY:
    plugin->notify_port = (LV2_Atom_Sequence *)data;
    break;
  case PORT_OUT_L:
    plugin->output_port_l = (float *)data;
    break;
  case PORT_OUT_R:
    plugin->output_port_r = (float *)data;
    break;
  case PORT_LATENCY:
    plugin->output_latency = (float *)data;
    break;
  default:
    if (port < PORT_LAST) {
      plugin->input_ports[port] = (float *)data;
    }
    break;
  }
}

static LV2_Handle instantiate(const LV2_Descriptor *descriptor, double rate,
                              const char *path,
                              const LV2_Feature *const *features) {
  struct sampler_plugin *plugin =
      (struct sampler_plugin *)calloc(1, sizeof(struct sampler_plugin));

  humanizer_settings_init(&plugin->hsettings, (int)rate);
  plugin->sampler = sampler_new(NUM_VOICES, (int)rate, &plugin->hsettings);
  plugin->samplerate = (int)rate;

  // Get host features
  const char *missing = lv2_features_query(
      features,
      LV2_LOG__log, &plugin->logger.log, false,
      LV2_URID__map, &plugin->map, true,
      LV2_WORKER__schedule, &plugin->schedule, true,
      NULL);
  lv2_log_logger_set_map(&plugin->logger, plugin->map);
  if (missing) {
    lv2_log_error(&plugin->logger, "Missing feature <%s>\n", missing);
    free(plugin);
    return NULL;
  }

  map_sampler_uris(plugin->map, &plugin->uris);
  lv2_atom_forge_init(&plugin->forge, plugin->map);

  return (LV2_Handle)plugin;
}

static void cleanup(LV2_Handle instance) {
  struct sampler_plugin *plugin = (struct sampler_plugin *)instance;
  sampler_free(plugin->sampler);
  free(plugin);
}

static void handle_event(struct sampler_plugin *plugin, LV2_Atom_Event *ev) {
  plugin_uris *uris = &plugin->uris;

  // Handle MIDI note events.
  if (ev->body.type == uris->midi_Event) {
    const uint8_t *const msg = (const uint8_t *)(ev + 1);
    int note, velocity;
    
    switch (lv2_midi_message_type(msg)) {
    case LV2_MIDI_MSG_NOTE_ON:
      note = (int)msg[1];
      velocity = (int)msg[2];
      sampler_noteoff(plugin->sampler, note);
      sampler_noteon(plugin->sampler, note, velocity);
      break;
    case LV2_MIDI_MSG_NOTE_OFF:
      note = (int)msg[1];
      //sampler_noteoff(plugin->sampler, note);
      break;
    default:                    // Make -Wall happy.
      break;
    }

  } else if (lv2_atom_forge_is_object_type(&plugin->forge, ev->body.type)) {
    const LV2_Atom_Object *obj = (const LV2_Atom_Object *)&ev->body;
    if (obj->body.otype == uris->patch_Set) {
      // Get the property and value of the set message
      const LV2_Atom* property = NULL;
      const LV2_Atom* value    = NULL;
      lv2_atom_object_get(obj,
                          uris->patch_property, &property,
                          uris->patch_value,    &value,
                          0);
      if (!property) {
        lv2_log_error(&plugin->logger, "Set message with no property\n");
        return;
      } else if (property->type != uris->atom_URID) {
        lv2_log_error(&plugin->logger, "Set property is not a URID\n");
        return;
      }

      const uint32_t key = ((const LV2_Atom_URID*)property)->body;
      if (key == uris->eg_drumkit) {
        // Sample change, send it to the worker.
        lv2_log_trace(&plugin->logger, "Scheduling sample change\n");
        plugin->schedule->schedule_work(plugin->schedule->handle,
                                        lv2_atom_total_size(&ev->body),
                                        &ev->body);
      /* } else if (key == uris->param_gain) { */
      /*   // Gain change */
      /*   if (value->type == uris->atom_Float) { */
      /*     plugin->gain = DB_CO(((LV2_Atom_Float*)value)->body); */
      /*   } */
      }

    } else if (obj->body.otype == uris->patch_Get && plugin->sampler->kit) {
      // Received a get message, emit our state (probably to UI)
      lv2_atom_forge_frame_time(&plugin->forge, plugin->frame_offset);
      write_set_file(&plugin->forge, &plugin->uris,
                     plugin->sampler->kit->path,
                     strlen(plugin->sampler->kit->path));
    }
  }
}

static void update_hsettings(struct sampler_plugin *plugin) {
  // Apply settings to sampler.
  if (*plugin->input_ports[PORT_HUMANIZE_ENABLE] > 0) {
    plugin->hsettings.latency_max_ms = *plugin->input_ports[PORT_HUMANIZE_LATENCY_MAX];
    plugin->hsettings.latency_stddev_ms = *plugin->input_ports[PORT_HUMANIZE_LATENCY_STDDEV];
    plugin->hsettings.latency_laid_back_ms = *plugin->input_ports[PORT_HUMANIZE_LATENCY_LAID_BACK];
    plugin->hsettings.latency_regain = *plugin->input_ports[PORT_HUMANIZE_LATENCY_REGAIN];
    plugin->hsettings.velocity_stddev = *plugin->input_ports[PORT_HUMANIZE_VELOCITY_STDDEV];
  } else {
    plugin->hsettings.latency_max_ms = 0;
    plugin->hsettings.latency_stddev_ms = 0;
    plugin->hsettings.latency_laid_back_ms = 0;
    plugin->hsettings.latency_regain = 1;
    plugin->hsettings.velocity_stddev = 0;
  }
}

static void render(struct sampler_plugin *plugin, uint32_t start,
                   uint32_t end) {
  update_hsettings(plugin);
  sampler_output(plugin->sampler,
                 plugin->output_port_l + start,
                 plugin->output_port_r + start,
                 end - start);
}

static void run(LV2_Handle instance, uint32_t sample_count) {
  struct sampler_plugin *plugin = (struct sampler_plugin *)instance;

  const uint32_t notify_capacity = plugin->notify_port->atom.size;
  lv2_atom_forge_set_buffer(&plugin->forge, (uint8_t *)plugin->notify_port,
                            notify_capacity);
  lv2_atom_forge_sequence_head(&plugin->forge, &plugin->notify_frame, 0);

  if (plugin->send_settings_to_ui) {
    plugin->send_settings_to_ui = 0;
    // Currently a no-op.
  }

  // Incrementally process control events and render audio data.
  plugin->frame_offset = 0;
  LV2_ATOM_SEQUENCE_FOREACH(plugin->control_port, ev) {
    render(plugin, plugin->frame_offset, ev->time.frames);

    plugin->frame_offset = ev->time.frames;

    handle_event(plugin, ev);
  }

  render(plugin, plugin->frame_offset, sample_count);

  if (plugin->output_latency) {
    *(plugin->output_latency) = (float)sampler_get_latency(plugin->sampler);
  }
}

static LV2_State_Status save(LV2_Handle instance,
                             LV2_State_Store_Function store,
                             LV2_State_Handle handle, uint32_t flags,
                             const LV2_Feature *const *features) {
  struct sampler_plugin *plugin = (struct sampler_plugin *)instance;
  LV2_State_Map_Path *map_path;
  const char *apath;

  if (!plugin->sampler->kit) {
    return LV2_STATE_SUCCESS;
  }

  map_path =
      (LV2_State_Map_Path *)lv2_features_data(features, LV2_STATE__mapPath);
  if (!map_path) {
    return LV2_STATE_ERR_NO_FEATURE;
  }

  apath = map_path->abstract_path(map_path->handle, plugin->sampler->kit->path);

  store(handle, plugin->uris.eg_drumkit, apath, strlen(apath) + 1,
        plugin->uris.atom_Path, LV2_STATE_IS_POD | LV2_STATE_IS_PORTABLE);

  free((void *)apath);
  return LV2_STATE_SUCCESS;
}

static LV2_State_Status restore(LV2_Handle instance,
                                LV2_State_Retrieve_Function retrieve,
                                LV2_State_Handle handle, uint32_t flags,
                                const LV2_Feature *const *features) {
  struct sampler_plugin *plugin = (struct sampler_plugin *)instance;
  LV2_Worker_Schedule *schedule = NULL;
  LV2_State_Map_Path *paths = NULL;
  const char *path, *missing;
  const void *value;
  size_t size;
  uint32_t type;
  uint32_t valflags;

  // Get host features
  missing = lv2_features_query(features, LV2_STATE__mapPath, &paths, true,
                               LV2_WORKER__schedule, &schedule, false, NULL);
  if (missing) {
    lv2_log_error(&plugin->logger, "Missing feature <%s>\n", missing);
    return LV2_STATE_ERR_NO_FEATURE;
  }

  value = retrieve(handle, plugin->uris.eg_drumkit, &size, &type, &valflags);
  if (!value) {
    lv2_log_error(&plugin->logger, "Missing eg:drumkit\n");
    return LV2_STATE_ERR_NO_PROPERTY;
  } else if (type != plugin->uris.atom_Path) {
    lv2_log_error(&plugin->logger, "Non-path eg:drumkit\n");
    return LV2_STATE_ERR_BAD_TYPE;
  }

  path = paths->absolute_path(paths->handle, (const char *)value);

  if (!plugin->activated || !schedule) {
    struct drumkit *new_kit;

    lv2_log_trace(&plugin->logger, "Synchronous restore\n");

    // Do sync restore.
    new_kit = drumkit_new(path, plugin->samplerate);
    if (!new_kit) {
      lv2_log_error(&plugin->logger, "Error loading the drumkit %s\n", path);
      return LV2_WORKER_ERR_UNKNOWN;
    }
    sampler_set_drumkit(plugin->sampler, new_kit);
  } else {
    LV2_Atom_Forge forge;
    LV2_Atom *buf = (LV2_Atom *)calloc(1, strlen(path) + 128);

    lv2_log_trace(&plugin->logger, "Asynchronous restore\n");
    lv2_atom_forge_init(&forge, plugin->map);
    lv2_atom_forge_set_sink(&forge, atom_sink, atom_sink_deref, buf);
    write_set_file(&forge, &plugin->uris, path, strlen(path));

    schedule->schedule_work(plugin->schedule->handle,
                            lv2_atom_pad_size(buf->size), buf + 1);
    free(buf);
  }

  free((void *)path);
  plugin->send_settings_to_ui = 1;

  return LV2_STATE_SUCCESS;
}

static const void *extension_data(const char *uri) {
  static const LV2_State_Interface state = {save, restore};
  static const LV2_Worker_Interface worker = {work, work_response, NULL};
  if (!strcmp(uri, LV2_STATE__interface)) {
    return &state;
  } else if (!strcmp(uri, LV2_WORKER__interface)) {
    return &worker;
  }
  return NULL;
}

static const LV2_Descriptor descriptor = {
    EG_SAMPLER_URI,
    instantiate,
    connect_port,
    activate,
    run,
    deactivate,
    cleanup,
    extension_data
};

LV2_SYMBOL_EXPORT
const LV2_Descriptor *lv2_descriptor(uint32_t index) {
  switch (index) {
  case 0:
    return &descriptor;
  default:
    return NULL;
  }
}

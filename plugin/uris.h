
#ifndef __uris_H
#define __uris_H

#include "lv2/lv2plug.in/ns/ext/atom/atom.h"
#include "lv2/lv2plug.in/ns/ext/atom/forge.h"
#include "lv2/lv2plug.in/ns/ext/log/log.h"
#include "lv2/lv2plug.in/ns/ext/midi/midi.h"
#include "lv2/lv2plug.in/ns/ext/parameters/parameters.h"
#include "lv2/lv2plug.in/ns/ext/patch/patch.h"
#include "lv2/lv2plug.in/ns/ext/state/state.h"

#include <stdio.h>

#define EG_SAMPLER_URI "http://lv2.incal.net/plugins/hydrumkit"
#define EG_SAMPLER_UI_URI EG_SAMPLER_URI "#ui"

#define EG_SAMPLER__applyDrumkit EG_SAMPLER_URI "#applyDrumkit"
#define EG_SAMPLER__freeDrumkit EG_SAMPLER_URI "#freeDrumkit"
#define EG_SAMPLER__drumkit EG_SAMPLER_URI "#drumkit"
#define EG_SAMPLER__uiState EG_SAMPLER_URI "#UiState"

typedef struct {
  LV2_URID atom_Float;
  LV2_URID atom_Path;
  LV2_URID atom_Resource;
  LV2_URID atom_Sequence;
  LV2_URID atom_URID;
  LV2_URID atom_eventTransfer;
  LV2_URID eg_applyDrumkit;
  LV2_URID eg_freeDrumkit;
  LV2_URID eg_drumkit;
  LV2_URID midi_Event;
  LV2_URID patch_Get;
  LV2_URID patch_Set;
  LV2_URID patch_property;
  LV2_URID patch_value;
  LV2_URID ui_State;
} plugin_uris;

static inline void map_sampler_uris(LV2_URID_Map *map, plugin_uris *uris) {
  uris->atom_Float = map->map(map->handle, LV2_ATOM__Float);
  uris->atom_Path = map->map(map->handle, LV2_ATOM__Path);
  uris->atom_Resource = map->map(map->handle, LV2_ATOM__Resource);
  uris->atom_Sequence = map->map(map->handle, LV2_ATOM__Sequence);
  uris->atom_URID = map->map(map->handle, LV2_ATOM__URID);
  uris->atom_eventTransfer = map->map(map->handle, LV2_ATOM__eventTransfer);
  uris->eg_applyDrumkit = map->map(map->handle, EG_SAMPLER__applyDrumkit);
  uris->eg_freeDrumkit = map->map(map->handle, EG_SAMPLER__freeDrumkit);
  uris->eg_drumkit = map->map(map->handle, EG_SAMPLER__drumkit);
  uris->midi_Event = map->map(map->handle, LV2_MIDI__MidiEvent);
  uris->patch_Get = map->map(map->handle, LV2_PATCH__Get);
  uris->patch_Set = map->map(map->handle, LV2_PATCH__Set);
  uris->patch_property = map->map(map->handle, LV2_PATCH__property);
  uris->patch_value = map->map(map->handle, LV2_PATCH__value);
  uris->ui_State = map->map(map->handle, EG_SAMPLER__uiState);
}

/**
   Write a message like the following to `forge`:
   [source,n3]
   ----
   []
   a patch:Set ;
   patch:property eg:sample ;
   patch:value </home/me/foo.wav> .
   ----
*/
static inline LV2_Atom_Forge_Ref write_set_file(LV2_Atom_Forge *forge,
                                                const plugin_uris *uris,
                                                const char *filename,
                                                const uint32_t filename_len) {
  LV2_Atom_Forge_Frame frame;
  LV2_Atom_Forge_Ref set =
      lv2_atom_forge_object(forge, &frame, 0, uris->patch_Set);

  lv2_atom_forge_key(forge, uris->patch_property);
  lv2_atom_forge_urid(forge, uris->eg_drumkit);
  lv2_atom_forge_key(forge, uris->patch_value);
  lv2_atom_forge_path(forge, filename, filename_len);

  lv2_atom_forge_pop(forge, &frame);
  return set;
}

/**
   Get the file path from `obj` which is a message like:
   [source,n3]
   ----
   []
   a patch:Set ;
   patch:property eg:sample ;
   patch:value </home/me/foo.wav> .
   ----
*/
static inline const char *read_set_file(const plugin_uris *uris,
                                        const LV2_Atom_Object *obj) {
  if (obj->body.otype != uris->patch_Set) {
    fprintf(stderr, "Ignoring unknown message type %d\n", obj->body.otype);
    return NULL;
  }

  /* Get property URI. */
  const LV2_Atom *property = NULL;
  lv2_atom_object_get(obj, uris->patch_property, &property, 0);
  if (!property) {
    fprintf(stderr, "Malformed set message has no body.\n");
    return NULL;
  } else if (property->type != uris->atom_URID) {
    fprintf(stderr, "Malformed set message has non-URID property.\n");
    return NULL;
  } else if (((const LV2_Atom_URID *)property)->body != uris->eg_drumkit) {
    fprintf(stderr, "Set message for unknown property.\n");
    return NULL;
  }

  /* Get value. */
  const LV2_Atom *value = NULL;
  lv2_atom_object_get(obj, uris->patch_value, &value, 0);
  if (!value) {
    fprintf(stderr, "Malformed set message has no value.\n");
    return NULL;
  } else if (value->type != uris->atom_Path) {
    fprintf(stderr, "Set message value is not a Path.\n");
    return NULL;
  }

  return (const char *)LV2_ATOM_BODY_CONST(value);
}

#endif /* SAMPLER_URIS_H */

#ifndef __plugin_ports_H
#define __plugin_ports_H 1

// Definitions for ports to match our LV2 TTL spec.
#define PORT_CONTROL 0
#define PORT_NOTIFY 1
#define PORT_OUT_L 2
#define PORT_OUT_R 3
#define PORT_LATENCY 4
#define PORT_HUMANIZE_LATENCY_MAX 5
#define PORT_HUMANIZE_LATENCY_STDDEV 6
#define PORT_HUMANIZE_LATENCY_LAID_BACK 7
#define PORT_HUMANIZE_LATENCY_REGAIN 8
#define PORT_HUMANIZE_VELOCITY_STDDEV 9
#define PORT_HUMANIZE_ENABLE 10
#define PORT_LAST 11

#endif
